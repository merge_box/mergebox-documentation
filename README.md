# Merge Box documentation

This repository contains the Merge Box documentation as Markdown files.

## Adding a new language

Every directory not called `www` is interpreted as a language.

For translating, start by copying the english version. There, adjust the `book.toml` file: Change the language, title and authors. Then, you can start translating the individual Markdown files.

## Building

Since each individual language has its own book, one has to build every language individually. For assisting in this, there is a script called `build.sh`; set the environment variable `MDBOOK_OUTPUT__HTML__GIT_REPOSITORY_URL` to `https://gitlab.com/merge_box/mergebox-documentation` and run the script. The result will be placed into `www/`.
