#!/bin/sh

site_directory="$(pwd)/www"

export MDBOOK_BUILD__CREATE_MISSING=false
export MDBOOK_OUTPUT__HTML__GIT_REPOSITORY_ICON=fa-gitlab
if test -n "$MDBOOK_OUTOUT__HTML__GIT_REPOSITORY_URL"; then
	export MDBOOK_OUTPUT__HTML__EDIT_URL_TEMPLATE="$MDBOOK_OUTOUT__HTML__GIT_REPOSITORY_URL/-/edit/main/{path}"
fi

error() {
	echo "$1" >&2
	exit "$3"
}

for lang in */; do (
	if test "$lang" = 'www/'; then
		exit
	fi
	echo "> Building ${lang%/} ..."
	cd "$lang"
	lang_directory="$site_directory/$lang"
	mkdir -p -- "$lang_directory"
	if ! MDBOOK_BUILD__BUILD_DIR="$lang_directory" MDBOOK_OUTPUT__HTML__SITE_URL="/$lang" mdbook build; then
		error "! Failed" "$?"
	fi
); done
