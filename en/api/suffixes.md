# Suffixes

###### Suffix Definition
| Variable | Type     | Feature | Default value |
| -- | --| --| -- |
| suffix | string | name of suffix |               |
| value |  number | default Value | 1 |
| roundat | number | rounded to this number, if nil rounding isn't active | nil |
