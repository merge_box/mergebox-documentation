## Math Api

###### merge_box.functions.math.round(number)
Return the rounded `number` </br>
Returns the next integral value to `number`. If two integer values are equally close, it returns the higher one.

###### merge_box.functions.math.random(m, n)
Return a random value but runs before it math.randomseed(os.time()). Look at [https://www.lua.org/manual/5.4/manual.html#6.7](https://www.lua.org/manual/5.4/manual.html#6.7)